package com.example.myapplication;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;

public class BluetoothConnectionService {
    private static final String TAG= "BluetoothConnectionServ";

    private static final String appName= "MYAPP";

    private static final UUID MY_UIID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    private final BluetoothAdapter rBluetoothAdapter;
    Context nContext;
    private AcceptThread mInsecureAcceptThread;

    private ConnectThread mConnectThread;

    private BluetoothDevice mmDevice;

    private UUID deviceUUID;
    ProgressDialog nProgressDialog;

    private ConnectedThread mConnectedThread;

    public BluetoothConnectionService(Context context) {
        rBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        nContext = nContext;
        start();
    }

    private class AcceptThread extends Thread{
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;
        public AcceptThread(){
            BluetoothServerSocket tmp = null;
            //Create  a new Listening server socket.
            try{
                tmp= rBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(appName,MY_UIID_INSECURE);
                Log.d(TAG,"AcceptThread: Setting up Server using: "+MY_UIID_INSECURE);
            }catch (IOException e){

                Log.d(TAG,"AcceptThread: IOException: "+e.getMessage());

            }
            mmServerSocket=tmp;

        }
        public void run(){
            Log.d(TAG,"run: AcceptThread Running.");
            BluetoothSocket socket=null;
            try {
                //This is a blocking call and will only return on a
                // successful connection or an exception
                Log.d(TAG, "run: RFCOM server socket start........");
                socket = mmServerSocket.accept();
                Log.d(TAG, "run: RFCOM server socket accepted connection");
            }catch (IOException e)
            {
                Log.e(TAG,"AcceptThread: IOException: "+e.getMessage());
            }
            if (socket!=null)
            {
                connected(socket,mmDevice);

            }
            Log.i(TAG,"END mAcceptThread");
        }
        public void cancel(){
            Log.d(TAG,"cancel: Canceling AcceptThread");
            try{
                mmServerSocket.close();

            } catch (IOException e){
                Log.e(TAG, "cancel: Close of AcceptThread ServerSocket failed. "+e.getMessage());
            }
        }
    }



    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.( Se intentará conectar con otro dispositivo y muestra si lo logró o no.
     */
    private class ConnectThread extends Thread{
        private BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device,UUID uuid) {
            Log.d(TAG,"ConnectThread: started.");
            mmDevice=device;
            deviceUUID=uuid;
        }
        public void run(){
            BluetoothSocket tmp=null;
            Log.i(TAG,"RUN mConnectThread");
            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                Log.d(TAG,"ConnectThread: Trying to create InsecureRfcommSocket using UUID"+MY_UIID_INSECURE);
                tmp = mmDevice.createRfcommSocketToServiceRecord(deviceUUID);
            }catch (IOException e){
                Log.e(TAG,"ConnectThread: Could not create InsecureRfcommSocket"+e.getMessage());
            }
            mmSocket=tmp;
            //Always cancel discovery because it will show down a connection
            rBluetoothAdapter.cancelDiscovery();
            // Make a connection to the BluetoothSocket

            //This is a blocking call and will only return on a
            // successful connection or an exception
            try {
                mmSocket.connect();
                Log.d(TAG,"run: ConnectThread connected.");
        }catch (IOException e){
           //Close the socket.
                try {
                    mmSocket.close();
                    Log.d(TAG,"run: Closed Socket.");
                }
                catch  (IOException e1){
                    Log.e(TAG,"mConnectThread:run:  Unable to close connection in socket"+e1.getMessage());
                }
                Log.d(TAG,"run: ConnectThread: Could not connect to UUID: "+MY_UIID_INSECURE);
        }
            connected(mmSocket,mmDevice);
        }
        public void cancel(){

            try{
                Log.d(TAG,"cancel: Closing Client Socket");
                mmSocket.close();

            } catch (IOException e){
                Log.e(TAG, "cancel: close() of mmSocket in ConnectThread failed. "+e.getMessage());
            }
        }
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void start(){
        Log.d(TAG,"start");
        //Cancel any Thread attempting to make a connection
        if (mConnectThread !=null){
            mConnectThread.cancel();
            mConnectThread=null;
        }
        if (mInsecureAcceptThread==null){
            mInsecureAcceptThread= new AcceptThread();
            mInsecureAcceptThread.start();
        }
    }

    /**
     * AcceptThread starts and sits waiting for connection.
     * Then ConnectThread starts and attempts to make a connection with the other devices AcceptThread.
     */
    public void startClient(BluetoothDevice device, UUID uuid){
       Log.d(TAG, "startClient: Started.");
       //initprogress dialog
        nProgressDialog= ProgressDialog.show(nContext, "Connecting Bluetooth","Please Wait....",true);
        mConnectThread =new ConnectThread(device,uuid);
        mConnectThread.start();
    }
    private class ConnectedThread extends Thread{
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
           Log.d(TAG,"ConnectedThread: Strating");
           mmSocket=socket;
           InputStream tmpIn= null;
           OutputStream tmpOut= null;
           //dismiss the processdialog when connection is established(cuando exista conexión)
            nProgressDialog.dismiss();
            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            }catch(IOException e){

                e.printStackTrace();
            }
            mmInStream=tmpIn;
            mmOutStream=tmpOut;
        }
        public void run(){
            byte[] buffer= new byte[1024];//buffer (cuanto puede llegar a almacenar antes de perder datos.
            int bytes; //bytes que retorna por read()
            //Seguir leyendo hasta que ocurra una excepción
            while (true){
                try {
                    bytes = mmInStream.read(buffer);
                    String incomingMessage= new String(buffer,0,bytes);
                    Log.d(TAG,"InputStream: "+incomingMessage);
                }catch(IOException e)
                {
                    Log.e(TAG,"write: Error reading inputStream. "+e.getMessage());
                    break;
                }

            }
        }
        //Call this from the main Activity to send data to the remote device.*/
        public void write(byte[] bytes){
        String text=new String(bytes, Charset.defaultCharset());
        Log.d(TAG, "write: Writing to outputstream: "+text);
        try {
            mmOutStream.write(bytes);
        }catch (IOException e)
        {
            Log.e(TAG,"write: Error writing to outputsream. "+e.getMessage());
        }
        }


        /* Call this from the main Activity to shutdown the connection*/
        public void cancel(){
            try{
                mmSocket.close();
            }catch (IOException e){

            }
        }
    }
    private void connected(BluetoothSocket mmSocket, BluetoothDevice mmDevice) {
        Log.d(TAG,"connected: Starting");

        // Start the thread to manage the connection and perform transmission

        mConnectedThread = new ConnectedThread(mmSocket);
        mConnectedThread.start();
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The Bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write (byte[] out){
        // Create temporary object
        ConnectedThread r;
        //Synchronized a copy of the ConnectedThread
        Log.d(TAG,"write: Write Called.");
        //Perform the write.
        mConnectedThread.write(out);
    }

}
